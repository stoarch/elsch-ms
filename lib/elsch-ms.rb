require 'awesome_print'
require 'sinatra'
require 'sinatra/reloader'
require 'sequel'

set :port, 4570
#set :bind, '192.168.254.125'
set :bind, '185.48.149.157'

set :public_folder, 'static'

UNKNOWN_CHANEL = -1

ACTIVE_CHANEL = 0
REACTIVE_CHANLE = 1



$meters = [
    {name: 'l122', code: 11, pos: {x: 380, y: 480}, chanels: {active: 225, reactive: 226}},
    {name: 'l102', code: 12, pos: {x: 207, y: 480}, chanels: {active: 240, reactive: 241}},
    {name: 'l141', code: 13, pos: {x: 818, y: 678}, chanels: {active: 255, reactive: 256}},
    {name: 'l142', code: 14, pos: {x: 780, y: 485}, chanels: {active: 270, reactive: 271}},
    
    {name: 'lc-2', code: 15, pos: {x: 885, y: 485}, chanels: {active: 285, reactive: 286}},
    {name: 'lc-1', code: 16, pos: {x: 883, y: 678}, chanels: {active: 300, reactive: 301}},
    {name: 'l146', code: 17, pos: {x: 680, y: 485}, chanels: {active: 315, reactive: 316}},
    {name: 'AT-1', code: 18, pos: {x: 67, y: 482}, chanels: {active: 330, reactive: 331}},
    {name: 'AT-2', code: 19, pos: {x: 685, y: 678}, chanels: {active: 345, reactive: 346}},
    
    {name: 'l143', code: 20, pos: {x: 335, y: 678}, chanels: {active: 360, reactive: 361}},
    {name: 'ov-1', code: 21, pos: {x: 405, y: 678}, chanels: {active: 375, reactive: 376}},
    {name: 'ov-2', code: 22, pos: {x: 755, y: 678}, chanels: {active: 390, reactive: 391}},
    {name: 'tch1-21', code: 25, pos: {x: 550, y: 815}, chanels: {active: 435, reactive: 436}},
    {name: 'tch1-38', code: 26, pos: {x: 550, y: 753}, chanels: {active: 450, reactive: 451}},
    
    {name: 'tch2-52', code: 27, pos: {x: 548, y: 407}, chanels: {active: 465, reactive: 466}},
    {name: 'tch2-74', code: 28, pos: {x: 548, y: 350}, chanels: {active: 480, reactive: 481}},
    {name: 'tchr-2ra', code: 29, pos: {x: 310, y: 790}, chanels: {active: 495, reactive: 496}},
    {name: 'tchr-4rb', code: 30, pos: {x: 220, y: 790}, chanels: {active: 510, reactive: 511}},
    {name: 'l101', code: 102, pos: {x: 140, y: 675}, chanels: {active: 525, reactive: 526}},

    {name: 'l121', code: 103, pos: {x: 200, y: 675}, chanels: {active: 540, reactive: 541}},
    {name: 'l144', code: 202, pos: {x: 137, y: 482}, chanels: {active: 555, reactive: 556}},
    {name: 'lchrak1', code: 209, pos: {x: 63, y: 678}, chanels: {active: 585, reactive: 586}},
    {name: 'l195-30', code: 211, pos: {x: 950, y: 678}, chanels: {active: 600, reactive: 601}},
    {name: 'tg2',     code:  23, pos: {x: 485, y: 678}, chanels: {active: 405, reactive: 406}},

    {name: 'tg1',     code:  24, pos: {x: 485, y: 482}, chanels: {active: 420, reactive: 421}},
    {name: 'l196-32', code: 224, pos: {x: 970, y: 488}, chanels: {active: 720, reactive: 721}},   
    {name: 'l197-31', code: 239, pos: {x: 1015, y: 678}, chanels: {active: 915, reactive: 916}},
    {name: 'l83',     code: 214, pos: {x: 245, y: 355}, chanels: {active: 630, reactive: 631}},
    {name: 'l85',     code: 215, pos: {x: 245, y: 240}, chanels: {active: 645, reactive: 646}},

    {name: 'l86',     code: 216, pos: {x: 245, y: 185}, chanels: {active: 661, reactive: 662}},
    {name: 'l87',     code: 217, pos: {x: 245, y: 135}, chanels: {active: 675, reactive: 676}},
    {name: 'l90',     code: 218, pos: {x: 245, y:  25}, chanels: {active: 690, reactive: 691}},
    {name: 'l92',     code: 219, pos: {x: 245, y: -87}, chanels: {active: 705, reactive: 706}},
    {name: 'l84',     code: 225, pos: {x: 245, y: 299}, chanels: {active: 735, reactive: 736}},

    {name: 'l88',     code: 226, pos: {x: 245, y:  80}, chanels: {active: 750, reactive: 751}},
    {name: 'l91',     code: 227, pos: {x: 245, y: -30}, chanels: {active: 765, reactive: 766}},
    {name: 'l93',     code: 228, pos: {x: 245, y:-145}, chanels: {active: 780, reactive: 781}},
    {name: 'l98',     code: 229, pos: {x: 335, y: 358}, chanels: {active: 795, reactive: 796}},
    {name: 'l99',     code: 230, pos: {x: 335, y: 299}, chanels: {active: 810, reactive: 811}},

    {name: 'l100',    code: 231, pos: {x: 335, y: 240}, chanels: {active: 825, reactive: 826}},
    {name: 'l101',    code: 232, pos: {x: 335, y: 185}, chanels: {active: 840, reactive: 841}},
    {name: 'l103',    code: 233, pos: {x: 335, y: 132}, chanels: {active: 855, reactive: 856}},
    {name: 'l104',    code: 234, pos: {x: 335, y:  78}, chanels: {active: 870, reactive: 871}},
    {name: 'l105',    code: 235, pos: {x: 335, y:  25}, chanels: {active: 885, reactive: 886}},

    {name: 'l107',    code: 236, pos: {x: 335, y: -87}, chanels: {active: 900, reactive: 901}},
    {name: 'l102',    code: 240, pos: {x: 335, y:-145}, chanels: {active: 930, reactive: 931}},
    {name: 'l96',     code: 242, pos: {x: 335, y:-207}, chanels: {active: 960, reactive: 961}},
    ]

	$chanel_kind_codes = {
		"EAp"=> 0,
		"EAm"=> 1,
		"ERp"=> 2,
		"ERm"=> 3,
		"PowerR"=> 4,
		"PowerA"=> 5,
		"EnergRp"=> 6,
		"EnergRm"=> 7,
		"EnergAp"=> 8,
		"EnergAm"=> 9,
		"TokA"=> 10,
		"TokB"=> 11,
		"TokC"=> 12,
		"NaprA"=> 13,
		"NaprB"=> 14,
		"NaprC"=> 15,
		"Freq"=> 16,
		"Ku"=> 17,
		"CosFi"=> 18
	}

	$chanel_coefs = {
		"EAp"=> 1,
		"EAm"=> 1,
		"ERp"=> 1,
		"ERm"=> 1,
		"PowerR"=> 1e-6,
		"PowerA"=> 1e-6,
		"EnergRp"=> 1,
		"EnergRm"=> 1,
		"EnergAp"=> 1,
		"EnergAm"=> 1,
		"TokA"=> 1,
		"TokB"=> 1,
		"TokC"=> 1,
		"NaprA"=> 1,
		"NaprB"=> 1,
		"NaprC"=> 1,
		"Freq"=> 1,
		"Ku"=> 1,
		"CosFi"=> 1
	}

get '/' do
   connect_to_db

	 parm_code_active = params['code_active']
	 parm_code_reactive = params['code_reactive']

	 code_ids = [4,5]
	 code_active = $chanel_kind_codes[parm_code_active] unless parm_code_active.nil?
	 code_reactive = $chanel_kind_codes[parm_code_reactive] unless parm_code_reactive.nil?
	 
	 code_ids[ACTIVE_CHANEL] = code_active unless code_active.nil?
	 code_ids[REACTIVE_CHANLE] = code_reactive unless code_reactive.nil?

	 coef_act = 1
	 coef_react = 1

	 $coef_act = $chanel_coefs[parm_code_active] unless parm_code_active.nil?
	 $coef_react = $chanel_coefs[parm_code_reactive] unless parm_code_reactive.nil?

	 coefs = {active: $coef_act, reactive: $coef_react}

	 puts params
	 puts coefs

   receive_data(code_ids, coefs)

   $db.disconnect
   $db = nil 

   erb :ms
end

# methods #

def connect_to_db()
    $db ||= Sequel.connect('postgres://scaner:WinMainQ45@localhost:5432/elsch_hef')
end

def receive_data(code_ids, coefs)
    $values = {}
		puts coefs

    $meters.each do |m|
				m[:chanels][:active] = get_chanel_for(m[:code], code_ids[ACTIVE_CHANEL])[:id]
				m[:chanels][:reactive] = get_chanel_for(m[:code], code_ids[REACTIVE_CHANLE])[:id]

        next if m[:chanels][:active] == UNKNOWN_CHANEL
        next if m[:chanels][:reactive] == UNKNOWN_CHANEL

        active_ch_id = m[:chanels][:active]
        reactive_ch_id = m[:chanels][:reactive]

				coef_act = coefs[:active]
				coef_react = coefs[:reactive]

        $values[active_ch_id] = last_value_for( active_ch_id ) 
        $values[reactive_ch_id] = last_value_for( reactive_ch_id ) 

				puts $values
    end
end

def get_chanel_for( meter_id, kind )
	$db[:chanel_dict].where(meter_id: meter_id, chanel_kind: kind).first
end

def last_value_for( chanel )
  $db[:values].where( chanel_id: chanel ).order(:id).last
end
